package core;

import java.io.*;
// This class contains static methods for reading and writing
// fixed length records
class FixedLengthStringIO
{
  // Read fixed number of characters from a DataInput stream
  public static String readFixedLengthString(int size,
                                             DataInput in)
  throws IOException
  {
    char c[] = new char[size];

    for (int i=0; i<size; i++)
      c[i] = in.readChar();

    return new String(c);
  }

  // Write fixed number of characters (string s with padded spaces)
  // to a DataOutput stream
  public static void writeFixedLengthString(String s, int size,
    DataOutput out) throws IOException
  {
    char cBuffer[] = new char[size];
    s.getChars(0, s.length(), cBuffer, 0);
    for (int i=s.length(); i<cBuffer.length; i++)
      cBuffer[i] = ' ';
    String newS = new String(cBuffer);
    out.writeChars(newS);
  }
}
