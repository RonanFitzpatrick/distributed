// This program transmits data stored in text file within project folder.
// Better version would include user input to request a file based on what they need
package client;

import java.io.*;
import java.net.*;

public class RemoteFileClient
{
    public static void main(String[] args)
    {
        try
        {
            Socket client = new Socket("127.0.0.1", 3000);

            BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintWriter output = new PrintWriter(client.getOutputStream());

            // Send the name of the file to be read
            output.println("readFile.txt");
            output.flush();

            String line = null;
            System.out.println("*** File contents ***");
            // Read in the lines sent from the server
            while ((line = input.readLine()) != null)
            {
                System.out.println(line);
            }
            System.out.println("**** File ended ****");

            // Close the streams connected to the server
            output.close();
            input.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
