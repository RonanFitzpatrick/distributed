package threadPerClientServer;

import java.io.*;
import java.net.*;

public class ConnectionHandler implements Runnable
{

    protected Socket socketToHandle;

    public ConnectionHandler(Socket socketToHandle)
    {
        this.socketToHandle = socketToHandle;
    }

    public void run()
    {
        if (socketToHandle != null)

        {
            try
            {
                OutputStream outputToSocket = socketToHandle.getOutputStream();
                InputStream inputFromSocket = socketToHandle.getInputStream();

                PrintWriter streamWriter = new PrintWriter(outputToSocket);
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputFromSocket));

                String fileToRead = streamReader.readLine();
                BufferedReader fileReader = new BufferedReader(new FileReader(fileToRead));

                String line = null;
                while ((line = fileReader.readLine()) != null)
                {
                    streamWriter.println(line);
                }

                fileReader.close();
                streamWriter.close();
                streamReader.close();

            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
