package threadPerClientServer;

//package com.pracitalsockets;

import java.io.*;
import java.net.*;

public class RemoteFileServer
{
public static void main(String[] args)
{
    try
    {
        // Set up listening socket
        ServerSocket server = new ServerSocket(3000);
        
        while(true)
        {
            // Accept new client
            Socket clientLink = server.accept();
        
            // Create a new handler thread to deal with that client
            ConnectionHandler clientThread = new ConnectionHandler(clientLink);
            // Create a wrapper thread
            Thread t = new Thread(clientThread);
            // Kick off the thread handling this client
            t.start();
        }

    } catch (Exception e)
    {
        e.printStackTrace();
    }
}
}
