package threadPoolServer;
import java.io.*;
import java.net.*;

public class PooledRemoteFileServer
{
    protected int maxConnections;
    protected int listenPort;
    protected ServerSocket serverSocket;

    public PooledRemoteFileServer(int aListenPort, int maxConnections)
    {
        listenPort = aListenPort;
        this.maxConnections = maxConnections;
    }
    
    public static void main(String[] args)
    {
        // Set up server
        PooledRemoteFileServer server = new PooledRemoteFileServer(3000, 3);

        // Set up handler threads in pool
        server.setUpHandlers();
        // Start accepting connections
        server.acceptConnections();
    }
    
    public void acceptConnections()
    {
        try
        {
            // Open a listening socket
            ServerSocket server = new ServerSocket(listenPort, 5);
            Socket incomingConnection = null;

            // While we want to interact with more clients
            while (true)
            {
                // Accept next client
                incomingConnection = server.accept();
                // Handle that client, either by allocating to available thread or placing in queue
                handleConnection(incomingConnection);
            }
        }
        catch (BindException e)
        {
            System.out.println("Unable to bind to port " + listenPort);
        }
        catch (IOException e)
        {
            System.out.println("Unable to instantiate a ServerSocket on port: " + listenPort);
        }
    }
    protected void handleConnection(Socket connectionToHandle)
    {
        // Process the client's request to interact with the client
        PooledConnectionHandler.processRequest(connectionToHandle);
    }

    // Set up the connection pool - create all threads that will be reused during lifetime of server
    public void setUpHandlers()
    {
        for (int i = 0; i < maxConnections; i++)
        {
            PooledConnectionHandler currentHandler = new PooledConnectionHandler();
            Thread t = new Thread (currentHandler);
            t.start();
        }
    }
}
