package threadPoolServer;

import java.io.*;
import java.net.*;
import java.util.*;

public class PooledConnectionHandler implements Runnable
{

    // Specific Socket being worked on (client being worked with) by this thread
    protected Socket clientLink;
    // Create a list to store the sockets (representing clients) that have not yet been dealt with
    // static means ALL threads always share the same pool
    protected static List pool = new LinkedList();

    public PooledConnectionHandler()
    {
    }

    public static void processRequest(Socket incomingClient)
    {
        // Synchronize on the object, reduce the code that is locked at any time
        synchronized (pool)
        {
            // Add the new client to the pool
            pool.add(pool.size(), incomingClient);
            // Notify all waiting threads that there is a new client available to service
            // (like producer-consumer problem, available thread will consume new client socket)
            pool.notifyAll();
        }
    }

    public void run()
    {
        // Run forever
        while (true)
        {
            // Lock on the pool, if another thread is accessing the pool to either add a new client
            // or remove one to interact with, then we have to wait
            synchronized (pool)
            {
                // While there are no more client sockets in the pool,
                // wait for more to be added
                while (pool.isEmpty())
                {
                    try
                    {
                        // This will wait here until another client is added to the pool and notifyAll is called
                        pool.wait();
                    } catch (InterruptedException e)
                    {
                        return;
                    }
                }

                // When a new client has been added to the pool, 
                // remove it and work with it here ourselves
                clientLink = (Socket) pool.remove(0);
            }

            // Interact with client - do standard client interaction code
            handleConnection();
        }
    }
    
    public void handleConnection()
    {
        try
        {
            // Open streams to/from the client to allow communication
            PrintWriter output = new PrintWriter(clientLink.getOutputStream());
            BufferedReader input = new BufferedReader(new InputStreamReader(clientLink.getInputStream()));

            // Get the name of the file to read
            String fileToRead = input.readLine();
            // Open a connection to the file
            BufferedReader fileReader = new BufferedReader(new FileReader(fileToRead));

            String line = null;
            // While there are more lines to read in, send them to the client
            while ((line = fileReader.readLine()) != null)
            {
                output.println(line);
            }

            // Finished processing file - Close the connection to the file
            fileReader.close();
            // Close streams to client
            output.close();
            input.close();
            
        } catch (FileNotFoundException e)
        {
            System.out.println("Could not find requested file on the server.");
        } catch (IOException e)
        {
            System.out.println("Error handling a client: " + e);
        }
    }
}