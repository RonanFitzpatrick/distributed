package ServerServiceCode;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import ClientCode.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author grahamm
 */
public class AccountManager extends UnicastRemoteObject implements AccountManagerInterface 
{
    private List<BankClientInterface> activeClients = new ArrayList();
    private List<BankAccount> accounts = new ArrayList<BankAccount>();
    public AccountManager() throws RemoteException
    {
        super();
    }
    
    private BankAccount locateAccount(String firstName, String lastName, String password)
    {
        synchronized(accounts)
        {
        for(BankAccount b : accounts)
        {
            if(b.getFirstName().equals(firstName) && b.getLastName().equals(lastName) && b.getPassword().equals(password))
            {
                return b;
            }
        }
        }
        return null;
    }
    
    public boolean register(String firstName, String lastName, String address, String password) throws RemoteException
    {
        BankAccount b = new BankAccount(firstName, lastName, address, password);
        synchronized(accounts)
        {
        if(!accounts.contains(b))
        {
            
            accounts.add(b);
            return true;
        }
        else
        {
            return false;
        }
        }
    }
    
    public boolean login(String firstName, String lastName, String password) throws RemoteException
    {
        synchronized(accounts)
        {
        BankAccount b = locateAccount(firstName, lastName, password);
        if(b!= null)
        {
            b.setLoggedIn(true);
            return true;
        }
        else
        {
            return false;
        }
        }
    }
    
    @Override
    public boolean withdraw(double amount, String firstName, String lastName, String password) throws RemoteException
    {
        BankAccount b = locateAccount(firstName, lastName, password);
        
        if(b!= null)
        {
            
            if(b.withdraw(amount))
            {
                call_back_client("Withdraw succesful",firstName,lastName);
                return true;
            }
            else
            {
              call_back_client("Withdraw unsuccesful",firstName,lastName);

            }
            
        }
        
            return false;
        
    }
    
    @Override
    public boolean deposit(double amount, String firstName, String lastName, String password) throws RemoteException
    {
        BankAccount b = locateAccount(firstName, lastName, password);
        
        if(b!= null)
        {
            b.deposit(amount);
            return true;
        }
        else
        {
            return false;
        }
    }
    @Override
    public double getBalance(String firstName, String lastName, String password) throws RemoteException
    {
        BankAccount b = locateAccount(firstName, lastName, password);
        
        if(b!= null)
        {
            return b.getBalance();
        }
        else
        {
            return -1;
        }
    }
    
    @Override
    public double calcInterest(String firstName, String lastName, String password) throws RemoteException
    {
        synchronized(accounts)
        {
        BankAccount b = locateAccount(firstName, lastName, password);
        
        if(b!= null)
        {
            return b.calcInterest();
        }
        else
        {
            return -1;
        }
        }
    }


    @Override
    public boolean connect_client(BankClientInterface client) throws RemoteException
    {
        synchronized(activeClients)
        {
       if(!activeClients.contains(client))
       {
           call_back_all("A client has connected");
           return activeClients.add(client);
       }
        }
       return false;
    }

    @Override
    public boolean disconnect_client(BankClientInterface client) throws RemoteException
    {   
        synchronized(activeClients)
        {
           if(activeClients.remove(client))
           {
               
              
               call_back_all("A client has connected");
               return true;
           }
        }
           return false;
   
    }

    
public void call_back_all(String message) throws RemoteException
    {
        synchronized(activeClients)
        {
            for(BankClientInterface client : activeClients)
            {
                client.displayNotification(message);
            }
        }
    }  
    
    
   public void call_back_client(String firstName, String lastName, String message) throws RemoteException
    {
        synchronized(activeClients)
        {
            for(BankClientInterface client : activeClients)
            {
                if(client.getFirstName().equals(firstName) && client.getLastName().equals(lastName))
                {
                    client.displayNotification(message);
                    break;
                }
            }
        }
    }
}
