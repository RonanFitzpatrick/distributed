/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientCode;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author ronan
 */
public interface BankClientInterface extends Remote
{
    
    public void displayNotification(String message) throws RemoteException;
    public String getLastName() throws RemoteException;
    public String getFirstName() throws RemoteException;
}
