package server;

import core.Email;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class EmailStore
{
    private HashMap<String,ArrayList<Email>> emailList;

    public EmailStore()
    {
        emailList = new HashMap();
    }

    public void add(Email email)
    {
        ArrayList<Email> list = new ArrayList();
        if(emailList.containsKey(email.getTo()))
        {
            list = emailList.get(email.getTo());
            
        }
        list.add(email);
        emailList.put(email.getTo(),list);
    }
    
    public ArrayList<Email> getEmailsTo(String to)
    {
        if(emailList.containsKey(to))
        {
        return emailList.get(to);
        }
        return null;
    }

    public void clear()
    {
        emailList.clear();
    }

    public void size()
    {
        emailList.size();
    }

    public String toString()
    {
        return "EmailStore [emailList=" + emailList + "]";
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.emailList);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final EmailStore other = (EmailStore) obj;
        if (!Objects.equals(this.emailList, other.emailList))
        {
            return false;
        }
        return true;
    }
    
    

}
