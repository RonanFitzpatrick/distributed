package server;

import core.MySocket;
import core.Email;
import java.io.IOException;
import java.net.ServerSocket;

public class ServerApp
{

    private int serverPort = 5000;
    private EmailStore theEmailStore;

    public static void main(String[] args)
    {
        ServerApp serverApp = new ServerApp();
        serverApp.startApp();
    }

    public void startApp()
    {
        //used to store emails
        this.theEmailStore = new EmailStore();

        try
        {
            System.out.println("---- SERVER ----");
            // Create listening socket to accept connections through
            ServerSocket serverSocket = new ServerSocket(serverPort);
            System.out.println("Waiting...");
            // Accept next client
            String shutDown = "";
            while (!shutDown.equals("shutdown"))
            {
                MySocket client = new MySocket(serverSocket.accept());

                // Interact with client
                executeSession(client);
                client.close();
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void executeSession(MySocket client)
    {

        String message = "";
        while (!message.equals(-1))
        {
            // Do code for specific client's session
            message = client.receiveMessage();
            client.sendMessage(executeCommand(message));
        }
        // to do... 
    }

    private String executeCommand(String msg)
    {
        String[] msgArray = msg.split("%");
        if(msgArray[0].equals("send"))
        {
           theEmailStore.add(new Email(msgArray[1],msgArray[2],msgArray[4],msgArray[3]));
           return "Email Sent";
        }
        else if(msgArray[0].equals("retrieve"))
        {
            String emails = "=====================================================¬To:";
            theEmailStore.getEmailsTo(msgArray[1]);
            for(Email s: theEmailStore.getEmailsTo(msgArray[1]))
            {
                emails+= s.getFrom()+ "¬From:" + s.getTo() + "¬Subject:" + s.getSubject() +
                        "¬Body:" + s.getBody();
                emails += "¬======================================================¬";
            }
            return emails;
            
        }
        //to do...
       return ""; 
    }
}
