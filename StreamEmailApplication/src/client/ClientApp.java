package client;

import core.MySocket;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class ClientApp
{
    private int serverPort = 5000;
    private Scanner keyboard = new Scanner(System.in);
    private MySocket dataSocket;
    private String name;

    public static void main(String[] args)
    {
        ClientApp clientApp = new ClientApp();
        try
        {
            clientApp.startApp();
        } catch (UnknownHostException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void startApp() throws UnknownHostException, IOException
    {
        System.out.println("---- CLIENT ----");
        dataSocket = new MySocket("localhost", serverPort);
        send_recieve();
		//to do...
        dataSocket.close();
    }
    
    public void send_recieve()
    {
        System.out.println("Please enter your name");
        name = keyboard.nextLine();
        String command = "";
        
        
        while(!command.equals("-1"))
        {
            System.out.println("Please enter your command");
            command = keyboard.nextLine();
            if(command.equalsIgnoreCase("send"))
            {
              sendMessage();  
              System.out.println(dataSocket.receiveMessage());
            }
            else if(command.equalsIgnoreCase("retrieve"))
            {
              dataSocket.sendMessage("retrieve%"+name);
              String recieved = (dataSocket.receiveMessage());
              recieved = recieved.replaceAll("¬", "\n");
              System.out.println(recieved);
               
              
            } 
            
        }
        
    }
    
    public void sendMessage()
    {
        String send = "send%";
        System.out.println("Please enter who the email is to");
        String to =  keyboard.nextLine();
        System.out.println("Please enter the subject");
        String subject = keyboard.nextLine();
        System.out.println("Please enter the message");
        String message = keyboard.nextLine();
        System.out.println(send.replace("%", "¬"));
        send += name.replace("%","¬")+"%"+to.replace("%","¬")+"%"+subject.replace("%","¬")+"%"+message.replace("%","¬")+"%";
        
        dataSocket.sendMessage(send);
     
        
    }
    
    
    
    

}
