package ie.dkit.distributed.server.rooms;

import ie.dkit.distributed.server.rooms.message.ChatMessage;
import ie.dkit.distributed.server.rooms.message.MessageListener;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ChatRoomInterface extends Remote
{
    public boolean  addChatMessage(ChatMessage message) throws RemoteException;
    public int      getUserCount() throws RemoteException;
    public boolean  addMember(UUID uuid) throws RemoteException;
    public boolean  removeMember(UUID uuid) throws RemoteException;
    public boolean  registerListener(MessageListener listener) throws RemoteException;
}
