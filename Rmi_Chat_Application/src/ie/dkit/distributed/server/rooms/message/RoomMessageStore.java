package ie.dkit.distributed.server.rooms.message;

import java.util.ArrayList;
import java.util.Collections;

public class RoomMessageStore
{
    private final ArrayList<ChatMessage> messages;

    public RoomMessageStore()
    {
        this.messages = new ArrayList<>();
    }

    public synchronized boolean addMessage(ChatMessage message)
    {
        int index = Collections.binarySearch(messages, message);

        if(index >= 0)
        {
            this.messages.add(message);
            return true;
        }

        return false;
    }

    public synchronized ArrayList<ChatMessage> getMessages()
    {
        return new ArrayList<ChatMessage>(this.messages);
    }
}
