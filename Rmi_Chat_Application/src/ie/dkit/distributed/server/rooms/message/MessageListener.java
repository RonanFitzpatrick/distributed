package ie.dkit.distributed.server.rooms.message;

/**
 * Created by malachy on 17/12/15.
 */
public interface MessageListener
{
    public void MessageSent(ChatMessage chatMessage);
}
