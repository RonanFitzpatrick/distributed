package ie.dkit.distributed.server.rooms;

import ie.dkit.distributed.server.rooms.message.ChatMessage;
import ie.dkit.distributed.server.rooms.message.MessageListener;
import ie.dkit.distributed.server.rooms.message.RoomMessageStore;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.UUID;

public class ChatRoom extends UnicastRemoteObject  implements ChatRoomInterface, Serializable
{
    private static final long serialVersionUID = -8826728061664946693L;

    private final RoomMessageStore messages;
    private final ArrayList<UUID> members;
    private final ArrayList<MessageListener> listeners;

    protected ChatRoom() throws RemoteException
    {
        this.messages = new RoomMessageStore();
        this.members = new ArrayList<>();
        this.listeners = new ArrayList<>();
    }


    @Override
    public boolean addChatMessage(ChatMessage message) throws RemoteException
    {
        if(message != null)
        {
            boolean result =  this.messages.addMessage(message);

            if(result)
            {
                for (MessageListener listener : this.listeners)
                {
                    listener.MessageSent(message);
                }
            }

            return result;
        }

        return false;
    }

    @Override
    public int getUserCount()  throws RemoteException
    {
        return this.members.size();
    }

    @Override
    public boolean addMember(UUID uuid) throws RemoteException
    {
        if(uuid != null)
        {
            if( !(this.members.contains(uuid)) )
            {
                this.members.add(uuid);
                return true;
            }

        }

        return false;
    }

    @Override
    public boolean removeMember(UUID uuid) throws RemoteException
    {
        if (uuid != null)
        {
            if(this.members.contains(uuid))
            {
                this.members.remove(uuid);
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean registerListener(MessageListener listener) throws RemoteException
    {
        if(listener != null)
        {
            this.listeners.add(listener);
            return true;
        }

        return false;
    }
}
