package ie.dkit.distributed.server.rooms;

import ie.dkit.distributed.server.rooms.roomstorage.RoomStoreSingleton;
import java.io.Serializable;
import java.rmi.RemoteException;

public class MainChatRoomSingleton extends ChatRoom implements Serializable
{
    private static final long serialVersionUID = 1345050100408319287L;
    private static MainChatRoomSingleton instance;
    
    
    static
    {
        try
        {
            
            instance = new MainChatRoomSingleton();
            
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }
    

    public static synchronized MainChatRoomSingleton getInstance()
    {
        return instance;
    }
    public MainChatRoomSingleton() throws RemoteException
    {
        super();
    }
}
