package ie.dkit.distributed.server.rooms;



import java.io.Serializable;
import java.rmi.RemoteException;


public class MinorChatRoom extends ChatRoom implements Serializable
{
    private static final long serialVersionUID = 7813473051274850596L;

    public MinorChatRoom() throws RemoteException
    {
        super();
    }
}
