/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.distributed.server.session;


import static ie.dkit.distributed.server.session.SessionTokenFactory.generateSessionToken;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author Delwyn
 */
public class SessionMap extends UnicastRemoteObject implements Serializable, SessionMapInterface
{

    private final HashMap<String, UUID> SessionMap;
    private static SessionMap instance;
    
    
    static
    {
        try
        {
            instance = new SessionMap();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }

    
    public static SessionMap getInstance()
    {
        return instance;
    }
    
    
    private SessionMap() throws RemoteException
    {
         SessionMap = new HashMap<>();
      
    }
    
    @Override
    public String add(UUID uuid)
    {
        String token = generateSessionToken();
        
        while(SessionMap.containsKey(token))
        {
            token = generateSessionToken();
            if(!SessionMap.containsKey(token))
            {
                SessionMap.put(token, uuid);
            }
        }
        
        return token;
    }
    
    @Override
    public boolean expireSession(String token)
    {
        if(SessionMap.containsKey(token))
        {
            SessionMap.remove(token);
            return true;
        }
        
        return false;
    }
    
    @Override
    public UUID getUUID(String token)
    {
        return SessionMap.get(token);
    }
}
