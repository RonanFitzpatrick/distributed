package ie.dkit.distributed.server.session;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by malachy on 17/12/15.
 */
public class SessionTokenFactory
{
    //this is going to need access to the list/map of current session tokens to ensure that it does not generate the same
    //one twice.

    /**
     * Full disclosure, this String generation was taken from:
     * - http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
     *
     * @return
     */
    public static String generateSessionToken()
    {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

}
