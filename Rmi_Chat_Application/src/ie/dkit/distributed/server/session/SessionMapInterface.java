/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.distributed.server.session;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

/**
 *
 * @author ronan
 */
public interface SessionMapInterface extends Remote
{
   public String add(UUID uuid) throws RemoteException; 
   public boolean expireSession(String token) throws RemoteException;
   public UUID getUUID(String token) throws RemoteException;
   
}
