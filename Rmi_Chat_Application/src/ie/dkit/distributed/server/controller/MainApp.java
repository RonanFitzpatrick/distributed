package ie.dkit.distributed.server.controller;

import ie.dkit.distributed.server.rooms.MainChatRoomSingleton;
import ie.dkit.distributed.server.rooms.roomstorage.RoomStoreSingleton;
import ie.dkit.distributed.server.session.SessionMap;

import ie.dkit.distributed.server.userdata.UserStoreSingleton;
import ie.dkit.distributed.server.utility.Constants;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainApp
{
    private final RoomStoreSingleton minorChatRooms;
    private final UserStoreSingleton users;
    private final MainChatRoomSingleton mainChat;
    private final SessionMap sessions;
    private final ControlObject controller;

    public MainApp() throws RemoteException
    {
        super();

        this.minorChatRooms = RoomStoreSingleton.getInstance();
        this.users = UserStoreSingleton.getInstance();
        this.mainChat = MainChatRoomSingleton.getInstance();
        this.sessions = SessionMap.getInstance();
        this.controller = new  ControlObject(minorChatRooms, users, 
            sessions,mainChat);
    }

    public static void main(String[] args) throws MalformedURLException, RemoteException
    {
        MainApp theApp = new MainApp();
        try
        {
            theApp.start();
        }
        catch (RemoteException ex)
        {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void start() throws RemoteException, MalformedURLException
    {
        startRegistry(Constants.RMI_PORT);
        
        
        Naming.rebind(Constants.RMI_URL + "/controller",controller);
        System.out.println("Server registered.  Registry currently contains:");
        
        listRegistry(Constants.RMI_URL);

        while(true)
        {

        }
    }


    
    private void startRegistry(int RMIPortNum) throws RemoteException 
    {
        try 
        {
            Registry registry = LocateRegistry.getRegistry(RMIPortNum);
            registry.list();
        } 
        catch (RemoteException e)
        {
            Registry registry = LocateRegistry.createRegistry(RMIPortNum);
        }
    }
    
    private  void listRegistry(String registryURL) throws RemoteException, MalformedURLException 
    {
        
        String[] names = Naming.list(registryURL);
        for (int i = 0; i < names.length; i++) 
        {
            System.out.println(names[i]);
        }
    } 
}