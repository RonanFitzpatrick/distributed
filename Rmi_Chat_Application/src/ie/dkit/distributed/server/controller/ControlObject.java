package ie.dkit.distributed.server.controller;

import ie.dkit.distributed.server.database.DataStreamApi;
import ie.dkit.distributed.server.rooms.ChatRoom;
import ie.dkit.distributed.server.rooms.MainChatRoomSingleton;
import ie.dkit.distributed.server.rooms.roomstorage.RoomStoreSingleton;
import ie.dkit.distributed.server.session.SessionMap;
import ie.dkit.distributed.server.userdata.ChatMember;
import ie.dkit.distributed.server.userdata.UserStoreSingleton;
import ie.dkit.distributed.server.rooms.message.ChatMessage;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by malac on 20/12/2015.
 */
public class ControlObject extends UnicastRemoteObject  implements ObjectControllerInterface
{
    private final RoomStoreSingleton minorChatRooms;
    private final UserStoreSingleton users;
    private final SessionMap sessionMap;
    private final MainChatRoomSingleton mainChat;
    private final DataStreamApi database;

    public ControlObject(RoomStoreSingleton minorChatRooms, UserStoreSingleton users, 
            SessionMap sessionMap, MainChatRoomSingleton mainChat) throws RemoteException
    {
        this.minorChatRooms = minorChatRooms;
        this.users = users;
        this.sessionMap = sessionMap;
        this.mainChat = mainChat;
        this.database = new DataStreamApi();
    }

    @Override
    public String login(String username, String password)
    {
        if(username != null && password != null)
        {
            
            if(this.database.memberDetailsMatch(username, password))
            {
                UUID uuid = this.database.obtainUUID(username, password);
                String sessionKey = this.sessionMap.add(uuid);

                try
                {
                    this.users.addMember(uuid, new ChatMember(this.database.obtainMemberRank(uuid), username));
                    return sessionKey;
                }
                catch (RemoteException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
    
    @Override
    public boolean logout(String sessionKey)
    {
        return true;
    }
    
    @Override
    public void updateMessage(ChatMessage message,String room)
    {
        
    }
    
    @Override
    public void removeUser(ArrayList<ChatRoom> rooms,String sessionKey)
    {
        
    }
    
    @Override
    public boolean deleteUser(String username)
    {
        
       return true;
       
    }
    
           
}
