/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.distributed.server.controller;

import ie.dkit.distributed.server.rooms.ChatRoom;
import ie.dkit.distributed.server.rooms.message.ChatMessage;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;


/**
 *
 * @author ronan
 */
public interface ObjectControllerInterface extends Remote
{

    public String login(String username, String password) throws RemoteException;

    public boolean logout(String sessionKey)throws RemoteException;

    public void updateMessage(ChatMessage message, String room)throws RemoteException;

    public void removeUser(ArrayList<ChatRoom> rooms, String sessionKey)throws RemoteException;

    public boolean deleteUser(String username) throws RemoteException;

}
