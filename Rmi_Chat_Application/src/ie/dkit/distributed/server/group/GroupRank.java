package ie.dkit.distributed.server.group;

import java.awt.*;

public enum GroupRank
{

    /**
     * This data would preferably be stored in a CSV/JSON/XML file, with JSON being preferred.
     *
     * "How to parse JSON   - Java" - http://stackoverflow.com/questions/2591098/how-to-parse-json-in-java
     * Org.json library     - http://www.json.org/java/index.html
     *
     * Storing this in a text file means that lists or sets are going to need to be used instead of this enum implementation,
     * which is still incomplete.
     *
     * This may mean that you will need a GroupStore of some description, such that it's built like:
     *          HashMap<String, GroupRank> groupStore; The String being the group name or ID and the GroupRank being the
     *          object containing all the below information that you pulled in from a JSON file.
     */
    GUEST(1000, false, "Guest", Color.gray),
    MEMBER(999, false, "Member", Color.green),
    MODERATOR(50, false, "Moderator", Color.blue),
    ADMIN(25, false, "Administrator", Color.RED),
    SUPERADMIN(0, true, "Operator", Color.orange);

    private final int powerLevel;       //The group's "PowerLevel", lower is more.
    private final boolean superAdmin;   //The group's superAdmin status. This goes hand in hand with powerLevel.
    private final String displayName;   //The string that will be displayed in the list beside the text chat in each lobby.
    private final Color color;          //The color of the aforementioned string.

    private GroupRank(int powerLevel, boolean superAdmin, String displayName, Color color)
    {
        this.powerLevel = powerLevel;
        this.superAdmin = superAdmin;
        this.displayName = displayName;
        this.color = color;
    }

    /**
     * This method should return whether the first member is greater than, equal to, or less than the second member
     * given. Note that superAdmin status is a powerlevel of -1, meaning that it trumps anything else.
     *
     * @param first
     * @param second
     * @return
     */
    public static PowerStatus testPowerLevel(GroupRank first, GroupRank second)
    {
        if(first.getSuperAdminStatus() && second.getSuperAdminStatus())
        {
            return PowerStatus.EQUAL;
        }
        else if(first.getSuperAdminStatus())
        {
            return PowerStatus.GREATER_THAN;
        }
        else if(second.getSuperAdminStatus())
        {
            return PowerStatus.LESS_THAN;
        }
        else
        {
            if(first.getPowerLevel() > second.getPowerLevel())
            {
                return PowerStatus.GREATER_THAN;
            }
            else if(first.getPowerLevel() < second.getPowerLevel())
            {
                return PowerStatus.LESS_THAN;
            }
            else
            {
                return PowerStatus.EQUAL;
            }
        }
    }

    public boolean getSuperAdminStatus()
    {
        return this.superAdmin;
    }

    public int getPowerLevel()
    {
        return this.powerLevel;
    }
}