package ie.dkit.distributed.server.group;

public enum PowerStatus
{
    LESS_THAN, EQUAL, GREATER_THAN;
}
