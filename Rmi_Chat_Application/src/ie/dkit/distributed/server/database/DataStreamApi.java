package ie.dkit.distributed.server.database;

import ie.dkit.distributed.server.group.GroupRank;

import java.util.List;
import java.util.UUID;


public class DataStreamApi
{
    /**
     * This class is going to need to use a connection pool, or single, singleton based connection to do CRUD operations
     * on the database.
     *
     *
     */
    
    //REMOVE AFTER DB
    String person1 = "user1";
    String person2 = "user2";
    String password = "pass";
    UUID person1ID = UUID.randomUUID();
    UUID person2ID = UUID.randomUUID();
    //REMOVE
    public DataStreamApi()
    {
    }

    /**
     * When this method is called, there is going to need to be an even triggered or something that allows the UserStore
     * to update. Discuss once it is posting to the database.
     *
     *
     * @param name
     * @param password
     * @param email
     * @return
     */
    public boolean createNewMember(String name, String password, String email)
    {
        if(name != null && password != null && email != null)
        {

        }

        return false;
    }

    /**
     *
     * @param uuid
     * @return
     */
    public boolean deleteMember(UUID uuid)
    {
        if(uuid != null)
        {

        }

        return false;
    }

    /**
     *
     * @param groupRank
     * @return
     */
    public List<UUID> getGroups(GroupRank groupRank)
    {
        if(groupRank != null)
        {

        }
        return null;
    }
    
    public boolean memberDetailsMatch(String userName,String password)
    {
      
      return userName.equals(person1) || userName.equals(person2) && this.password.equals(password);
       
    }
    
    public UUID obtainUUID(String username,String password)
    {
       if(username.equals(person1)&&this.password.equals(password))
       {
           return person1ID;
       }
      return person2ID;
       
    }
    
    public GroupRank obtainMemberRank(UUID d)
    {
       return GroupRank.GUEST; 
    }
}
