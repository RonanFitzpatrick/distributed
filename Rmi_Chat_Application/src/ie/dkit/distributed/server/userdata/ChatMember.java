package ie.dkit.distributed.server.userdata;

import ie.dkit.distributed.server.group.GroupRank;


public class ChatMember
{
    private final GroupRank groupRank;
    private final String username;

    public ChatMember(GroupRank groupRank, String username)
    {
        this.groupRank = groupRank;
        this.username = username;
    }

    public String getUsername()
    {
        return this.username;
    }

    public GroupRank getGroupRank()
    {
        return this.groupRank;
    }
}
