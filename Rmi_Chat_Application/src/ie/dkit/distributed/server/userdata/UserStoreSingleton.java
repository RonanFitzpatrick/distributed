package ie.dkit.distributed.server.userdata;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * This class will ensure that there is only ever one instance of the userStore.
 * To use, simply create a reference where you need this object, like so:
 *
 * UserStoreSingleton userStore = UserStoreSingleton.getInstance();
 *
 * This way, you are using the <i>same</i> object as everyone else is using, ensuring that it is never duplicated;
 *
 */
public class UserStoreSingleton  implements Serializable, UserStoreInterface
{
    private static final long serialVersionUID = -3590851009707827317L;

    private final HashMap<UUID, ChatMember> memberCache;
    private final List<ConnectionListener> listeners;

    private static UserStoreSingleton instance;

    static
    {
        try
        {
            instance = new UserStoreSingleton();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }

    //Private maintains singleton status.
    private UserStoreSingleton() throws RemoteException
    {
        super();
        this.memberCache = new HashMap<>();
        this.listeners = new ArrayList<>();
    }

    public static UserStoreSingleton getInstance()
    {
        return instance;
    }

    @Override
    public boolean addMember(UUID uuid, ChatMember chatMember) throws RemoteException
    {
        if(uuid != null && chatMember != null)
        {
            this.memberCache.put(uuid, chatMember);

            for (ConnectionListener listener : this.listeners)
            {
                listener.userLogin(chatMember);
            }

            return true;
        }

        return false;
    }

    @Override
    public boolean removeMember(UUID uuid) throws RemoteException
    {
        if(uuid != null)
        {
            this.memberCache.remove(uuid);

            for (ConnectionListener listener : this.listeners)
            {
                listener.userLogout(uuid);
            }

            return true;
        }
        return false;
    }

    /**
     * https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html Documentation for Java 8 Optional. It
     * doesn't allow objects to be null, ever, it just defines whether or not they are present.
     *
     * @param uuid
     * @return
     */
    @Override
    public Optional<ChatMember> getMember(UUID uuid) throws RemoteException
    {
        if (uuid != null)
        {
            if (this.memberCache.containsKey(uuid))
            {
                return Optional.of(this.memberCache.get(uuid));
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean registerListener(ConnectionListener listener)
    {
        if(listener != null)
        {
            this.listeners.add(listener);
            return true;
        }

        return false;
    }
}