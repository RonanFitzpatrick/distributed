package ie.dkit.distributed.client.model;

import ie.dkit.distributed.server.rooms.message.*;

public class ChatMessage implements Comparable<ChatMessage>
{
    private final String from, message;
    private final long timeStamp;

    public ChatMessage(String from, long timeStamp, String message)
    {
        this.from = from;
        this.timeStamp = timeStamp;
        this.message = message;
    }

    public String getFrom()
    {
        return from;
    }

    public String getMessage()
    {
        return message;
    }

    public long getTimeStamp()
    {
        return this.timeStamp;
    }

    @Override
    public int compareTo(ChatMessage o)
    {
        return Long.compare(this.timeStamp, o.getTimeStamp());
    }
}
