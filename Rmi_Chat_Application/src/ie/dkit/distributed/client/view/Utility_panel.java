/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.distributed.client.view;

import ie.dkit.distributed.client.view.Gui;
import java.awt.Component;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author ronan
 */
public class Utility_panel extends java.awt.Panel
{
    protected Gui get_gui() throws NotBoundException, MalformedURLException, RemoteException
    {
       Component reference_container = this;
        
        while(!reference_container.getClass().isInstance(new Gui()))
        {
            reference_container = reference_container.getParent();
        }
        
        return (Gui)reference_container;
    }
}
