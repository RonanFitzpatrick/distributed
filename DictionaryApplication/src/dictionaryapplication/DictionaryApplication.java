/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionaryapplication;
import java.util.*;
/**
 *
 * @author Ronan
 */
public class DictionaryApplication
{
    private HashMap<String,WordEntry> dictionary;
    private String owner;
    private int highestRetrieved;

    public DictionaryApplication(String owner)
    {
        this.owner = owner;
        this.dictionary = new HashMap<>();
    }
    public boolean add(WordEntry word)
    {
        String key = word.getWord();
        if(!this.dictionary.containsKey(key))
        {
        dictionary.put(key, word);
        return true;
        }
        return false;
    }
    public WordEntry mostDefintions()
    {
        WordEntry most = null;
        int currentHighest = -1;
        
        for (WordEntry value : dictionary.values())
        {
            if(value.getDefinitions().size() > currentHighest)
            {
                most = value;
            }
        }
        return most;
    }
    
    
    public boolean remove(String word)
    {
        if(dictionary.containsKey(word))
        {
            dictionary.remove(word);
            return true;      
        }
        return false;
    }
    
    public WordEntry search(String word)
    {
        
        WordEntry retrieved = dictionary.get(word);
        if(retrieved != null)
        {
            retrieved.updateCount();
            if (retrieved.getRetrievedCount() > highestRetrieved)
            {
                highestRetrieved++;
            }
        }
        return retrieved;
          
    }
    
    public ArrayList<WordEntry> mostRetrieved()
    {
        ArrayList<WordEntry> words = new ArrayList<WordEntry>();
        for (WordEntry currentWord : dictionary.values())
        {
            if(currentWord.getRetrievedCount() == highestRetrieved)
            {
                words.add(currentWord);
            }
        }
        
        return words;
    }

    public HashMap<String, WordEntry> getDictionary()
    {
        return dictionary;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setDictionary(HashMap<String, WordEntry> dictionary)
    {
        this.dictionary = dictionary;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    @Override
    public String toString()
    {
        return "DictionaryApplication{" + "dictionary=" + dictionary + ", owner=" + owner + '}';
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
    }
    
}
