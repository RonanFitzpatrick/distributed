
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionaryapplication;


import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ronan
 */
public class DictionaryServer 
{
    private static HashMap<String,DictionaryApplication> dictionarys;
    
    public static void main(String[] args)
    {
       
        dictionarys = new HashMap<>();
        DictionaryApplication app = new DictionaryApplication("DicOne");
        dictionarys.put("DicOne", app);
        try {
        InetAddress receiverHost;
        int receiverPort;
        int myPort = Integer.parseInt("10313");
        MyDatagramSocket mySocket = new MyDatagramSocket(myPort);
        while(true)
        {
             DatagramPacket datagram = mySocket.receiveMessage();
             receiverHost = datagram.getAddress();
             receiverPort = datagram.getPort();             
             String recieved= new String (datagram.getData());
             mySocket.sendMessage(receiverHost, receiverPort, options(recieved.trim()));
            
        }
        
        }
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        
    }
    
    private static String options(String recieved)
    {
        String output = "Invalid Command";
        System.out.println(recieved);
        if(recieved.startsWith("add"))
        {
            output = add(recieved);
        }
        else if(recieved.startsWith("search"))
        {
            output = search(recieved);
        }
        else if(recieved.startsWith("remove"))
        {
            System.out.println("HELLO");
            output = delete(recieved);
        }
        else if(recieved.startsWith("stats"))
        {
            output = stats();
        }
        return output;
    }
    
    private static String stats()
    {
        
        return "Owner:\t\t" + dictionarys.get("DicOne").getOwner()+ "\n"
                +"Most Definitions\t:" + dictionarys.get("DicOne").mostDefintions()+"\n"
                + "Most retrieved \t" + dictionarys.get("DicOne").mostRetrieved()+"\n";
                
        
    }
    
    private static String add(String recieved)
    {
        System.out.println(recieved);
        ArrayList<String> list = RegexUtility.get(recieved,"¬([^¬]+)", 1);
        String s = list.get(0);
        list = RegexUtility.get(list.get(1),"([^;]+)", 1);
        WordEntry word = new WordEntry(s,list);
        if(dictionarys.get("DicOne").add(word))
            return "Added";
        
        else
            return "Failed";
            
    }
    
    private static String search(String recieved)
    {
        if (dictionarys.get("DicOne").getDictionary().containsKey(RegexUtility.get(recieved,"¬([^¬]+)", 1).get(0)))
        {
        return dictionarys.get("DicOne").search(RegexUtility.get(recieved,"¬([^¬]+)", 1).get(0)).toString();
        }
        return "not found";
    }
    
    private static String delete(String recieved)
    {
        
        String s = "¬([A-Za-z]+)";
        
        System.out.println(RegexUtility.getFirst(recieved, s, 1));
        if(dictionarys.get("DicOne").getDictionary().containsKey(RegexUtility.getFirst(recieved, s, 1)))
        {
        if(dictionarys.get("DicOne").remove(RegexUtility.getFirst(recieved, s, 1)) == true)
            return "Removed";
        }
        return "not removed";
       
    }
        
        
    
    
    
}
