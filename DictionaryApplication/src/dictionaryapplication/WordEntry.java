/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dictionaryapplication;
import java.util.*;
/**
 *
 * @author Ronan
 */
public class WordEntry
{
    private String word;
    private int retrievedCount;
    private ArrayList<String> definitions;

    public WordEntry(String word, ArrayList<String> definitions)
    {
        this.word = word;
        this.retrievedCount = 0;
        this.definitions = definitions;
    }
    
    public void updateCount()
    {
        retrievedCount++;
    }

    public String getWord()
    {
        return word;
    }

    public int getRetrievedCount()
    {
        return retrievedCount;
    }

    public ArrayList<String> getDefinitions()
    {
        return definitions;
    }

    @Override
    public String toString()
    {
        return "WordEntry{" + "word=" + word + ", retrievedCount=" + retrievedCount + ", definitions=" + definitions + '}';
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.word);
        hash = 89 * hash + this.retrievedCount;
        hash = 89 * hash + Objects.hashCode(this.definitions);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final WordEntry other = (WordEntry) obj;
        if (!Objects.equals(this.word, other.word))
        {
            return false;
        }
        if (this.retrievedCount != other.retrievedCount)
        {
            return false;
        }
        if (!Objects.equals(this.definitions, other.definitions))
        {
            return false;
        }
        return true;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public void setRetrievedCount(int retrievedCount)
    {
        this.retrievedCount = retrievedCount;
    }

    public void setDefinitions(ArrayList<String> definitions)
    {
        this.definitions = definitions;
    }
    
    
    
}
