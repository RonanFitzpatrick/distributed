/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionaryapplication;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Ronan
 */
public class MainApp
{

    private final String BREAKCHARCTER = "¬";

    public static void main(String args[])
    {
        MainApp theApp = new MainApp();

        theApp.start();
    }

    public void start()
    {

        int choice = 0;

        while (choice != 5)
        {
            printMain();
            choice = MyScanner.getInt("Please enter a valid choice\n", 1, 5);
            chosenFunction(choice);
        }
        //end of run loop   
    }

    public void printMain()
    {
        System.out.println("1:Add");
        System.out.println("2:Search");
        System.out.println("3:Stats");
        System.out.println("4:Remove");
    }

    public void chosenFunction(int choice)
    {
        switch (choice)
        {
            case 1:
                add();
                break;
            case 2:
                search();
                break;
            case 3:
                stats();
                break;
            case 4:
                remove();
                break;

        }

    }

    public void stats()
    {
        System.out.println(sendRecieve("stats"));
    }

    public void search()
    {
        System.out.println(sendRecieve("search" + BREAKCHARCTER + MyScanner.getString("Please enter the "
                + "word you wish to search for\n")));
    }

    public void add()
    {

        System.out.println(sendRecieve("add" + BREAKCHARCTER + MyScanner.getString("Please enter the "
                + "word you wish to add\n") + BREAKCHARCTER + MyScanner.getString("Please enter it's defenition, after each "
                        + "definition put a semi-colon\n") + BREAKCHARCTER));

    }

    public void remove()
    {
        System.out.println("1:Remove Word\n2:Remove definition\n");
        if (MyScanner.getInt("Choose a valid menu option\n", 1, 2) == 1)
        {
            removeEntry();
        } 
        else
        {
            removeDef();
        }

    }

    public void removeDef()
    {
        //needs work
        sendRecieve(sendRecieve("search" + BREAKCHARCTER
                + MyScanner.getString("Please enter the word you wish that has the "
                        + "defenition you wish to remove\n")));
    }

    public void removeEntry()
    {
        sendRecieve("remove" + BREAKCHARCTER + MyScanner.getString("Please enter the "
                + "word you wish to remove\n"));
    }

    public String sendRecieve(String message)
    {
        try
        {

            InetAddress receiverHost = InetAddress.getByName("localhost");
            int receiverPort = Integer.parseInt("10313");
            int myPort = Integer.parseInt("1211");

            MyDatagramSocket mySocket = new MyDatagramSocket(myPort);
            mySocket.sendMessage(receiverHost, receiverPort, message);

            DatagramPacket datagram = mySocket.receiveMessage();
            message = new String(datagram.getData());
            mySocket.close();

        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return message;
    }
}
