package dictionaryapplication;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
	
public class RegexUtility 
{
	//returns true if the string matches the regular expression, otherwise false
	public static boolean matches(String strData, String strRegex)
	{
		return strData.matches(strRegex);
	}
	
	//prints all matches for the regex group == 0 
	public static void find(String strData, String strRegex)
	{
		//print matches for the entire regex
		find(strData, strRegex, 0); 
	}
	
	//prints all matches for the regex group specified 
	public static void find(String strData, 
			String strRegex, int groupNumber)
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);
		
		while(match.find())
			System.out.println(match.group(groupNumber));
	}
	
	public static void find(Pattern pattern, String strData, int groupNumber)
	{
		Matcher match = pattern.matcher(strData);

		while(match.find())
		{
				System.out.println(match.group(groupNumber));
		}
	}
	
	//returns an ArrayList of all matches for the regex group specified 
	public static ArrayList<String> get(String strData,
			String strRegex, int groupNumber)
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);	
		
		ArrayList<String> list = new ArrayList<String>();
		
		while(match.find())
			list.add(match.group(groupNumber));
		
		return list;
	}
	
	//returns the first match for the regex group specified 
	public static String getFirst(String strData,
			String strRegex, int groupNumber)
	{
		Pattern pattern = Pattern.compile(strRegex);
		Matcher match = pattern.matcher(strData);	
		
		if(match.find())
			return match.group(groupNumber);
		else
			return null;
	}
}








