/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.rmi.Remote;
import java.util.List;

/**
 *
 * @author ronan
 */
public interface EmailInterface extends Remote  //must extend**
{
    //all methods throws java.rmi.remoteexcpetion
    public Boolean sendEmail(Email email) throws java.rmi.RemoteException;
    public List<Email> viewSentEmails(String type) throws java.rmi.RemoteException;
    public List<Email> viewRecievedEmails(String type) throws java.rmi.RemoteException;

  
    
}
