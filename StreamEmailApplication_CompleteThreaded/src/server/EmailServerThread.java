package server;

import core.Email;
import core.EmailUtility;
import core.MySocket;
import java.util.ArrayList;

/**
 *
 * @author Michelle
 */
public class EmailServerThread implements Runnable
{
    private MySocket client;
    private int id;
    private EmailStore theEmailStore;   // This variable will be shared between all threads. 
                                        // Therefore we need to synchronize access
    
                                        // Best place to synchronize in a situation like this is 
                                        // within the EmailStore itself as we have full control of that class
    
    // As we have global state, i.e. information spanning the lifetime of the 
    // server and shared across all clients, we need to take it in alongside
    // the socket connected to the client and the id for this client
    public EmailServerThread(MySocket link, int id, EmailStore e)
    {
        this.client = link;
        this.id = id;
        this.theEmailStore = e;
    }
    
    public void run()
    {
        // Do code for specific client's session
        System.out.println("New client (id= " + id + ") on " + client.getRemoteAddress());

        String message = "";
        // While client is still interacting
        while(!message.equals(EmailUtility.EXIT))
        {
            // Receive command
            message = client.receiveMessage();
            String response = executeCommand(message);
            client.sendMessage(response);
        }
    }

    private String executeCommand(String msg)
    {
        String[] msgArray = msg.split(EmailUtility.COMMAND_BREAKING_CHAR);
        String response = "";
        if(msgArray[0].equals(EmailUtility.SEND_MAIL))
        {
            //Parse and store email
            Email e = EmailUtility.parseEmail(msgArray[1]);
            if(e != null)
            {
                theEmailStore.add(e);
                response = EmailUtility.SUCCESSFUL_SEND;
            }
            else
            {
                response = EmailUtility.FAILED_SEND;
            }
        }
        else if(msgArray[0].equals(EmailUtility.VIEW_MAIL) || msgArray[0].equals(EmailUtility.VIEW_SENT))
        {
            ArrayList<Email> mails = null;
            if(msgArray[0].equals(EmailUtility.VIEW_MAIL))
            {
                // Search for and send back received email
                mails = theEmailStore.getReceivedEmails(msgArray[1]);
            }
            else
            {
                // Search for and send back sent email
                mails = theEmailStore.getSentEmails(msgArray[1]);
            }
            
            if(mails.isEmpty())
            {
                response = EmailUtility.NO_MAIL;
            }
            else
            {
                // Format message to send back to client
                StringBuffer sb = new StringBuffer();
                sb.append(mails.get(0).getTransmisionFormat());
                for(int i = 1; i < mails.size(); i++)
                {
                    sb.append(EmailUtility.EMAIL_SEPARATOR_CHAR);
                    sb.append(mails.get(i).getTransmisionFormat());
                }
                response = sb.toString();
            }
        }
        else if(msgArray[0].equals(EmailUtility.EXIT))
        {
            response = EmailUtility.EXIT_ACCEPTED;
        }
        return response;
    }
}
