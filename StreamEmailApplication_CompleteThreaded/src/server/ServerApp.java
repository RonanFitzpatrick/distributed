package server;

import core.EmailUtility;
import core.MySocket;
import java.net.ServerSocket;

public class ServerApp
{
    private EmailStore theEmailStore;

    public static void main(String[] args)
    {
        ServerApp serverApp = new ServerApp();
        serverApp.startApp();
    }

    public void startApp()
    {
        //used to store emails
        this.theEmailStore = new EmailStore();

        try
        {
            System.out.println("---- SERVER ----");
            // Create listening socket to accept connections through
            ServerSocket serverSocket = new ServerSocket(EmailUtility.SERVER_PORT);
            System.out.println("Waiting...");
            int id = 1;
            while(true)
            {
                // Accept next client
                MySocket client = new MySocket(serverSocket.accept());

                // Create an instance of the Thread to handle the accepted client
                EmailServerThread serverThread = new EmailServerThread(client, id++, theEmailStore);
                // Wrap it in a Thread (as EmailServerThread implements Runnable it does not have a start() method)
                Thread wrapper = new Thread(serverThread);
                // Kick off the thread
                wrapper.start();
                // No need for the other methods in this class now, all functionality 
                // to do with handling a client is transferred into the thread.
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
