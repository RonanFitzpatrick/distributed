/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author ronan
 */
public class ServerRmi
{

    public static void main(String args[]) throws RemoteException, MalformedURLException
    {
        EmailRmi export = new EmailRmi();
        int portNum = 1088;
        startRegistry(portNum);
        // register the object under the name “some”
        String registryURL = "rmi://localhost:" + portNum + "/EmailRmi";
        Naming.rebind(registryURL, export);

        
    }
    
    
    private static void startRegistry(int RMIPortNum) throws RemoteException
    {
        try
        {
            Registry registry = LocateRegistry.getRegistry(RMIPortNum);
   // The above call will throw an exception if
            // the registry does not already exist
            registry.list();
        } catch (RemoteException ex)
        {
            // No valid registry at that port.
            System.out.println("RMI registry cannot be located at port "
                    + RMIPortNum);
            // Make a registry on that port.
            Registry registry = LocateRegistry.createRegistry(RMIPortNum);
            System.out.println("RMI registry created at port " + RMIPortNum);
        }
    } // end startRegistry

}
