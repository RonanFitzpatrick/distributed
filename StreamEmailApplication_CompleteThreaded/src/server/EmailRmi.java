/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.Email;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ronan
 */
public class EmailRmi extends UnicastRemoteObject implements core.EmailInterface
{
    private ArrayList<Email> emailList = new ArrayList();
    public EmailRmi() throws RemoteException
    {
        super();
    }

    @Override
    public Boolean sendEmail(Email email) throws RemoteException
    {
        emailList.add(email);
        return true;
    }

    @Override
    public List<Email> viewSentEmails(String name) throws RemoteException
    {
        ArrayList returnList = new ArrayList();
        for(Email mail: emailList)
        {
            if(mail.getFrom().equals(name))
            {
                returnList.add(mail.getBody());
            }
        }
        return returnList;
    }

    @Override
    public List<Email> viewRecievedEmails(String name) throws RemoteException
    {
               ArrayList returnList = new ArrayList();
        for(Email mail: emailList)
        {
            if(mail.getTo().equals(name))
            {
                returnList.add(mail.getBody());
            }
        }
        return returnList;
    }
    
}
