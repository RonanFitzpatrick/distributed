package client;
import java.rmi.*;
import core.*;

public class RMIClient {
   public static void main(String args[]) 
{
     try 
     {
	String registryURL = "rmi://localhost:" + 1088 + "/EmailRmi"; 
        EmailInterface h = (EmailInterface)Naming.lookup(registryURL);                
        // invoke the remote method(s)
        boolean message = h.sendEmail(new Email("john","harry","hello","hello"));
        String mes2 = h.viewRecievedEmails("harry").toString();
        System.out.println(mes2);
        System.out.println(message);
        // method2 can be invoked similarly
     } // end try 
     catch (Exception e) 
     {
         System.out.println("Exception in SomeClient: " + e);
     } 
   } //end main
   // Definition for other methods of the class, if any.
}//end class
