package client;

import core.Email;
import core.EmailUtility;
import core.MySocket;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ClientApp
{
    private Scanner keyboard = new Scanner(System.in);
    private MySocket dataSocket;
    
    private final String hardCodedPassword = "PASSWORD";
    private final String hardCodedUsername = "USERNAME";
    
    private String username;

    public static void main(String[] args)
    {
        ClientApp clientApp = new ClientApp();
        try
        {
            clientApp.startApp();
        } catch (UnknownHostException e)
        {
            System.out.println("Problem occured when finding host");
            e.printStackTrace();
        } catch (IOException e)
        {
            System.out.println("Problem occured when interacting with server");
            e.printStackTrace();
        }
    }

    public void startApp() throws UnknownHostException, IOException
    {
        System.out.println("---- CLIENT ----");
        System.out.println("Welcome to the EmailClient\n--- LOGIN ----");
        int selection = -1;
        dataSocket = new MySocket("localhost", EmailUtility.SERVER_PORT);

        String password;
        
        do
        {
            System.out.println("Please enter your username: ");
            username = keyboard.nextLine();
            System.out.println("Please enter your password: ");
            password = keyboard.nextLine();
            // Check would be done over network in full version of application
        }while(!username.equals(hardCodedUsername) && !password.equals(hardCodedPassword));
        
        System.out.println("You have successfully logged in");
	// loop interaction with server until user decides to exit
        while(selection != 4)
        {
            selection = chooseMenuOption();
            switch(selection){
                case 1: 
                {
                    sendMail();
                    break;
                }
                case 2: 
                {
                    viewReceivedMail();
                    break;
                }
                case 3: 
                {
                    viewSentMail();
                    break;
                }
                case 4: 
                {
                    dataSocket.sendMessage(EmailUtility.EXIT);
                    String response = dataSocket.receiveMessage();
                    
                    if(!response.equals(EmailUtility.EXIT_ACCEPTED))
                    {
                        System.out.println("A problem occurred while attempting to exit. Try again later.");
                        selection = -1;
                    }
                    break;
                }
            }
        }
        dataSocket.close();
        
        System.out.println("Thank you for using the EmailClient");
    }

    public int chooseMenuOption()
    {
        int option = -1;
        
        do
        {
            System.out.println("--------------------------------");
            System.out.println("1) Send email");
            System.out.println("2) View received email");
            System.out.println("3) View sent email");
            System.out.println("4) Exit email client");
            System.out.println("--------------------------------");
            System.out.println("Please select an option from the menu above");

            try{
                option = keyboard.nextInt();
                keyboard.nextLine();
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number between 1 and 4 as indicated by the menu above.");
                option = -1;
                keyboard.nextLine();
                pauseDisplay();
            }
        }while(option < 1 || option > 4);
        
        return option;    
    }

    public void sendMail()
    {
        // Take in information
        System.out.print("Recipient name:> ");
        String recipient = keyboard.nextLine();
        System.out.print("Message subject:> ");
        String subject = keyboard.nextLine();
        System.out.print("Message body:> ");
        String body = keyboard.nextLine();
        
        // Send message to server
        // Protocol message: send¬¬sender¬¬recipient¬¬subject¬¬body
        // Protocol possible responses: SUCCESSFUL | FAILED
        String message = EmailUtility.SEND_MAIL + EmailUtility.COMMAND_BREAKING_CHAR + username + EmailUtility.EMAIL_COMPONENT_BREAKING_CHAR + recipient + EmailUtility.EMAIL_COMPONENT_BREAKING_CHAR + subject + EmailUtility.EMAIL_COMPONENT_BREAKING_CHAR + body; 
        dataSocket.sendMessage(message);
        
        // Receive response from server
        String response = dataSocket.receiveMessage();
        
        // Process response
        if(response.equals(EmailUtility.SUCCESSFUL_SEND))
        {
            System.out.println("Message sent.");
        }
        else if(response.equals(EmailUtility.FAILED_SEND))
        {
            System.out.println("Message sending failed");
        }
        else
        {
            System.out.println("An unknown issue occurred.");
        }
        
        pauseDisplay();
    }
    
    public void viewReceivedMail()
    {
        // Protocol message: getMail¬¬username
        // Protocol possible responses: sender¬¬recipient¬¬subject¬¬body[~~sender¬¬recipient¬¬subject¬¬body] | NONE
        dataSocket.sendMessage(EmailUtility.VIEW_MAIL + EmailUtility.COMMAND_BREAKING_CHAR + username);
        String response = dataSocket.receiveMessage();
        String [] breakdown = response.split(EmailUtility.EMAIL_SEPARATOR_CHAR);
        if(!breakdown[0].equals(EmailUtility.NO_MAIL))
        {
            System.out.println("Received mail: ");
            ArrayList<Email> mails = EmailUtility.parseEmails(breakdown);
            for(Email e: mails)
            {
                System.out.println(e.getViewFormat());
                pauseDisplay();
            }
        }
        else
        {
            System.out.println("No received email found.");
            pauseDisplay();
        }
    }
    
    public void viewSentMail()
    {
        // Protocol message: getSent%%%username
        // Protocol possible responses: sender¬¬recipient¬¬subject¬¬body~~[sender¬¬recipient¬¬subject¬¬body] | NONE
        dataSocket.sendMessage(EmailUtility.VIEW_SENT + EmailUtility.COMMAND_BREAKING_CHAR + username);
        String response = dataSocket.receiveMessage();
        String [] breakdown = response.split(EmailUtility.EMAIL_SEPARATOR_CHAR);
        if(!breakdown[0].equals(EmailUtility.NO_MAIL))
        {
            System.out.println("Sent mail: ");
            ArrayList<Email> mails = EmailUtility.parseEmails(breakdown);
            for(Email e: mails)
            {
                System.out.println(e.getViewFormat());
                pauseDisplay();
            }
        }
        else
        {
            System.out.println("No sent email found.");
            pauseDisplay();
        }
        
    }
    
    public void pauseDisplay()
    {
        System.out.println("Press enter to continue");
        keyboard.nextLine();
    }
}
