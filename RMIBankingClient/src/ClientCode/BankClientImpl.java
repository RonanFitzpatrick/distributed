/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientCode;

import java.rmi.RemoteException;

/**
 *
 * @author ronan
 */
public class BankClientImpl implements BankClientInterface
{
    private String firstName;
    private String lastName;
    
   public BankClientImpl(String firstName, String lastName) throws RemoteException
   {
       super();
       this.firstName = firstName;
       this.lastName = lastName;
       
   }

    @Override
    public void displayNotification(String message) throws RemoteException
    {
        System.out.println("Message from server " + message);
    }

    @Override
    public String getFirstName() throws RemoteException
    {
        return this.firstName;
    }

    @Override
    public String getLastName() throws RemoteException
    {
        return this.lastName;
    }
    
}
