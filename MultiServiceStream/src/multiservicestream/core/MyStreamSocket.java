package multiservicestream.core;

import java.net.*;
import java.io.*;

public class MyStreamSocket extends Socket
{
    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;

    // Constructor that takes in a specific address (ip and port) to connect to
    public MyStreamSocket(String acceptorHost, int acceptorPort) throws SocketException, IOException
    {
        // Create a new DATA socket connected to the given address
        socket = new Socket(acceptorHost, acceptorPort);
        // Set up the input and output streams for sending and receiving data
        setStreams();
    }

    // Constructor that takes in an existing (already connected) DATA socket 
    public MyStreamSocket(Socket socket) throws IOException
    {
        // Store the supplied socket
        this.socket = socket;
        // Set up the input and output streams for sending and receiving data
        setStreams();
    }

    private void setStreams() throws IOException
    {
        // INPUT
        // Get the input stream for reading from this DATA socket
        InputStream inStream = socket.getInputStream();
        // Create a BufferedReader (could also be a Scanner) to read incoming data from input stream
        input = new BufferedReader(new InputStreamReader(inStream));
        
        // OUTPUT
        // Get the output stream for writing to this DATA socket
        OutputStream outStream = socket.getOutputStream();
        // Create a PrinterWriter to write data out to output stream
        output = new PrintWriter(new OutputStreamWriter(outStream));
    }

    public void sendMessage(String message) throws IOException
    {
        output.println(message);
        // Call flush() to ensure the data sent is all fully written to the socket data output stream
        output.flush();
    } // end sendMessage

    public String receiveMessage() throws IOException
    {
        // read a line from the data stream
        String message = input.readLine();
        return message;
    } //end receiveMessage

    public void close() throws IOException
    {
        socket.close();
    }
} //end class
