package multiservicestream.requestor;

import multiservicestream.core.MyStreamSocket;
import java.util.Scanner;

public class Example5ConnectionRequestor
{
    /*
     *	An application that requests a connection and receives a message using a stream-mode socket.
     */
    public static void main(String[] args)
    {
        // Take in information from the user
        Scanner input = new Scanner(System.in);

        // Take in the IP address to connect to
        System.out.println("Please enter the IP address to connect to:");
        String hostname = input.nextLine();

        // Take in the port number to connect to
        System.out.println("Please enter the port number this application should attempt to connect to:");
        int portNo = input.nextInt();

        try
        {
            
            // Try to create a data socket connected to the accepting process at the specified IP and port
            // This will fail if there is no listening socket in place
            MyStreamSocket mySocket = new MyStreamSocket(hostname, portNo);
            System.out.println("Connection request granted");
            System.out.println("Waiting to read");
            String message = "";
            while(!message.equals("shutdown"))
            {
            message = input.nextLine();
            // Wait for a message from the acceptor and display it when it arrives
            mySocket.sendMessage(message);
            message = mySocket.receiveMessage();
            System.out.println("Message received:");
            System.out.println(message);
            
            }
            mySocket.close();
            // Message has been received, close the DATA socket, notify the user and end the program
            
            System.out.println("Data socket closed.");
            // There is no CONNECTION socket on the requestor side, so no more sockets to close.
        } // end try
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    } // end main
} // end class
