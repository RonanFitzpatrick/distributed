package multiservicestream.acceptor;

import multiservicestream.core.MyStreamSocket;
import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

public class Example5ConnectionAcceptor
{
    /*
     *	An application that accepts a connection and sends a message using stream-mode sockets.
     */

    public static void main(String[] args)
    {
        // Take in information from the initiator
        Scanner input = new Scanner(System.in);

        // Take in the port number to accept connections on
        System.out.println("Please enter the port number this application should accept connections on:");
        int portNo = input.nextInt();
        // Clear Scanner's buffer
        input.nextLine();

        // Take in the message that should be sent back to requestors

       
        try
        {
            // Set up the connection point
            // Create a socket for accepting connections (CONNECTION socket)
            String message ="";
            ServerSocket connectionSocket = new ServerSocket(portNo);
            System.out.println("Now ready to accept a connection.");

            // wait to accept a connecrion request, at which time a DATA socket is created                  
            // (accept() method returns a DATA socket connected to the requestor)
            MyStreamSocket dataSocket = new MyStreamSocket(connectionSocket.accept());
            System.out.println("Connection accepted.");
            while(!message.equals("shutdown"))
            {
            
            
            message = dataSocket.receiveMessage();
            if(message.startsWith("echo"))
            {
            // Use the data socket to send a message
                message = message.substring(5, message.length());
            }
            else if(message.equals("daytime"))
            {
                    Calendar c1 = Calendar.getInstance();
                    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    message = date.format(c1.getTime());
            }
            else if(message.equals("random"))
            {
                Random rn = new Random();
                message = "" + rn.nextInt(1000) + 1; 
                
            }
            dataSocket.sendMessage(message);
            


            }
            // Message has been sent, close the DATA socket & notify the user of this program
            dataSocket.close();
            System.out.println("Data socket closed.");
            

            // Data socket has been closed, now close the CONNECTION socket, notify the user and end the program
            
            System.out.println("Connection socket closed.");
            connectionSocket.close();
        } // end try
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        
        
         // end catch
    } // end main
} // end class