package clientCode;

// Example: TCP (connection-oriented) socket Client
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SampleClient
{
    private static final int PORT = 22517;

    public static void main(String args[])
    {
        // the main processing
        Socket link = null;

        try
        {
            link = new Socket("localhost", PORT);

            // Set up communication streams
            BufferedReader in = new BufferedReader(new InputStreamReader(link.getInputStream()));
            PrintWriter out = new PrintWriter(link.getOutputStream(), true);

            Scanner userEntry = new Scanner(System.in);

            String message;
            String response = "";

            do
            {
                System.out.println("Please enter message to be echoed: (Enter \".\" to close)");
                message = userEntry.nextLine();
                out.println(message);
                response = in.readLine();
                
                System.out.println("Server response:> " + response);
            } while (!response.equals("GOODBYE"));
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                System.out.println("\n* Closing connection... *");
                link.close();
            } catch (IOException e)
            {
                System.out.println("Unable to disconnect!");
                System.exit(1);
            }
        }
    }
}
