package serverCode;

import java.io.*;
import java.net.*;
import java.util.*;

public class SampleServer
{
    private static ServerSocket servSock;
    private static final int PORT = 22517;

    public static void main(String args[])
    {
        int numClients = 0;
        System.out.println("Service running on port " + PORT + "\n");
        
        try
        {
            servSock = new ServerSocket(PORT);
            ThreadGroup group = new ThreadGroup("Client threads");
            // Place more emphasis on accepting clients than processing them 
            // by setting their maximum priority to be 1 less than the main method's priority
            group.setMaxPriority(Thread.currentThread().getPriority() - 1);
            
            do
            {
                Socket clientLink = servSock.accept();
                System.out.println("The server has now accepted " + ++numClients + " clients");
                // When dealing with state, pass variables to be shared in to each thread via constructor
                SampleServerThread t1 = new SampleServerThread(group, clientLink, numClients);
                t1.start();
            } while (true);
        } catch (IOException e)
        {
            System.out.println("Unable to attach to port!");
            System.exit(1);
        }
    }
}
