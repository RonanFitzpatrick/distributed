package serverCode;

//import the I/O classes required for stream use
import java.io.*;
//import the networking classes required for Inter-Process Communications
import java.net.*;

import java.util.*;

public class SampleServerThread extends Thread
{
    // Create the global variables to hold the Socket, streams and number of this thread
    private Socket link;
    private BufferedReader in;
    private PrintWriter out;
    private int number;

    // Constructor should pass in any information the thread will need to interact with the client
    // It MUST get the client's Socket object
    // If there is an object to be shared between all clients 
    // (e.g. Dictionary or EmailStore all clients should add to or query),
    // then it should be passed into the Thread via the constructor's parameters
    public SampleServerThread(ThreadGroup tg, Socket dataSocket, int number)
    {
        // Assign this thread to the supplied group
        super(tg, ("Thread " + number));
        
        try
        {
            // Save the data socket used for communication between the thread and the Client
            link = dataSocket;
            // Save the id of the thread to identify output
            this.number = number;
            // Create the stream for reading from the Client
            in = new BufferedReader(new InputStreamReader(link.getInputStream()));
            // Create the stream for writing to the Client
            out = new PrintWriter(link.getOutputStream(), true);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // run method. This is called when .start() is called on an instance of the Thread class
    // Should contain the LOGIC of the server, i.e. 
    //      a) receiving messages from client, 
    //      b) processing what the client wants to do
    //      c) sending an appropriate response back to the client
    
    public void run()
    {
        try
        {
            // Create logging variables
            int numMessages = 0;	//Keep track of the number of messages received from the client
            String response = "";
            String message;

            while (!response.equals("GOODBYE"))
            {
                message = in.readLine();
                System.out.println("/FROM CLIENT " + number + ": Message received: " + message);
                String[] content = message.split("%");
                if (content[0].equals("echo") && content.length == 2)
                {
                    response = content[1];
                    numMessages++;
                }
                else if(content[0].equals("."))
                {
                    response = "GOODBYE";
                }
                else 
                {
                    response = "INVALID_COMMAND";
                }

                out.println(response);
            }
            System.out.println("Final number of echo requests received from client " + number + ": " + numMessages);
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                System.out.println("\n* Closing connection with client " + number + "... *");
                link.close();				    //Step 5.
            } catch (IOException e)
            {
                System.out.println("Unable to disconnect!");
                System.exit(1);
            }
        }
    }
}
