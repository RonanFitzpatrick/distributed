package ie.dkit.distributed.server.rooms.roomstorage;

import ie.dkit.distributed.server.rooms.MinorChatRoom;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.WeakHashMap;

public class RoomStoreSingleton extends UnicastRemoteObject implements Serializable, RoomStoreInterface
{
    private static final long serialVersionUID = -3047591682750917286L;

    private static RoomStoreSingleton instance;
    private final WeakHashMap<String, MinorChatRoom> rooms;

    static
    {
        try
        {
            instance = new RoomStoreSingleton();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }

    public static synchronized RoomStoreSingleton getInstance()
    {
        return instance;
    }

    private RoomStoreSingleton() throws RemoteException
    {
        super();
        this.rooms = new WeakHashMap<>();
    }

    @Override
    public synchronized boolean addRoom(String name, MinorChatRoom chatRoom) throws RemoteException
    {
        if(name != null && chatRoom != null)
        {
            if( !(this.rooms.containsKey(name)) )
            {
                this.rooms.put(name, chatRoom);
                return true;
            }
        }

        return false;
    }

    @Override
    public synchronized boolean deleteRoom(String name) throws RemoteException
    {
        if(name != null)
        {
            if(this.rooms.containsKey(name))
            {
                this.rooms.remove(name);
                return true;
            }
        }

        return false;
    }
}
