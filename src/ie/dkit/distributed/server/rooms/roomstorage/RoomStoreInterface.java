package ie.dkit.distributed.server.rooms.roomstorage;

import ie.dkit.distributed.server.rooms.MinorChatRoom;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by malachy on 17/12/15.
 */
public interface RoomStoreInterface extends Remote
{
    public boolean addRoom(String name, MinorChatRoom chatRoom) throws RemoteException;
    public boolean deleteRoom(String name) throws RemoteException;
}
