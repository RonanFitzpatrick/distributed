package ie.dkit.distributed.server.rooms;

import ie.dkit.distributed.server.rooms.message.ChatMessage;
import ie.dkit.distributed.server.rooms.message.RoomMessageStore;

import java.io.Serializable;
import java.util.UUID;

public class MinorChatRoom extends ChatRoom implements Serializable
{
    private static final long serialVersionUID = 7813473051274850596L;

    public MinorChatRoom()
    {
        super();
    }
}
