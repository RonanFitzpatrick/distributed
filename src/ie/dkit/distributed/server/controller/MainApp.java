package ie.dkit.distributed.server.controller;

import ie.dkit.distributed.server.rooms.MainChatRoom;
import ie.dkit.distributed.server.rooms.roomstorage.RoomStoreSingleton;
import ie.dkit.distributed.server.userdata.ChatMember;
import ie.dkit.distributed.server.userdata.ConnectionListener;
import ie.dkit.distributed.server.userdata.UserStoreSingleton;
import ie.dkit.distributed.server.utility.Constants;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.UUID;

public class MainApp
{
    private final RoomStoreSingleton minorChatRooms;
    private final UserStoreSingleton users;
    private final MainChatRoom mainChat;

    public MainApp()
    {
        super();

        this.minorChatRooms = RoomStoreSingleton.getInstance();
        this.users = UserStoreSingleton.getInstance();
        this.mainChat = new MainChatRoom();
    }

    public static void main(String[] args)
    {
        MainApp theApp = new MainApp();
        theApp.start();
    }

    public void start()
    {
        instantiateRegistry();

        while(true)
        {

        }
    }

    public void instantiateRegistry()
    {
        try
        {
            Registry registry = LocateRegistry.getRegistry(Constants.RMI_PORT);
            registry.list();
        }
        catch (RemoteException e)
        {
            try
            {
                Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
            }
            catch (RemoteException e1)
            {
                e1.printStackTrace();
            }
        }
    }

    public void registerNetworkObjects() throws MalformedURLException, RemoteException
    {
        Naming.rebind(Constants.RMI_URL + "/rooms", this.minorChatRooms);
        Naming.rebind(Constants.RMI_URL + "/users", this.users);
        Naming.rebind(Constants.RMI_URL + "/mainchat", this.mainChat);
    }
}
