package ie.dkit.distributed.server.utility;

/**
 * Created by malachy on 17/12/15.
 */
public class Constants
{
    public static final int RMI_PORT = 13493;
    public static final String RMI_URL = "rmi://localhost:" + RMI_PORT;

}
