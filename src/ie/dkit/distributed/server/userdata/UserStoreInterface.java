package ie.dkit.distributed.server.userdata;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by malachy on 17/12/15.
 */
public interface UserStoreInterface extends Remote
{
    public boolean addMember(UUID uuid, ChatMember chatMember) throws RemoteException;
    public boolean removeMember(UUID uuid) throws RemoteException;
    public Optional<ChatMember> getMember(UUID uuid) throws RemoteException;
    public boolean registerListener(ConnectionListener listener);
}
