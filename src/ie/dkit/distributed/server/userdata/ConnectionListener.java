package ie.dkit.distributed.server.userdata;

import java.util.UUID;

/**
 * Created by malachy on 17/12/15.
 */
public interface ConnectionListener
{
    public void userLogin(ChatMember chatMember);
    public void userLogout(UUID uuid);
}
