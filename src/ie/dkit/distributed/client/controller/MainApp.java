package ie.dkit.distributed.client.controller;

import ie.dkit.distributed.client.model.RoomsJoined;
import ie.dkit.distributed.server.rooms.roomstorage.RoomStoreSingleton;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class MainApp
{
    private final RoomsJoined rooms;
    private final RoomStoreSingleton roomStore;

    public MainApp() throws RemoteException, NotBoundException, MalformedURLException
    {
        this.rooms = new RoomsJoined();
        this.roomStore = (RoomStoreSingleton) Naming.lookup("");//CHANGE ME!
    }

    public static void main(String[] args)
    {
        MainApp theApp = null;

        try
        {
            theApp = new MainApp();
        }
        catch (RemoteException | NotBoundException | MalformedURLException e)
        {
            e.printStackTrace();
        }

        theApp.start();
    }

    public void start()
    {

    }

}
