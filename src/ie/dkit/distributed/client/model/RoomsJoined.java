package ie.dkit.distributed.client.model;

import ie.dkit.distributed.server.rooms.ChatRoom;

import java.util.HashMap;

/**
 * Created by malachy on 17/12/15.
 */
public class RoomsJoined
{
    private final HashMap<String, ChatRoom> rooms;

    public RoomsJoined()
    {
        this.rooms = new HashMap<>();
    }

    public boolean addRoom(String roomName, ChatRoom room)
    {
        if(roomName != null && room != null)
        {
            if( !(this.rooms.containsKey(roomName)) )
            {
                this.rooms.put(roomName, room);
                return true;
            }
        }

        return false;
    }

    public boolean leaveRoom(String roomName)
    {
        if(roomName != null)
        {
            if(this.rooms.containsKey(roomName))
            {
                this.rooms.remove(roomName);
                return true;
            }
        }

        return false;
    }

}
