/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmigui;

import java.awt.Component;

/**
 *
 * @author ronan
 */
public class Utility_panel extends java.awt.Panel
{
    protected Gui get_gui()
    {
       Component reference_container = this;
        
        while(!reference_container.getClass().isInstance(new Gui()))
        {
            reference_container = reference_container.getParent();
        }
        
        return (Gui)reference_container;
    }
}
