/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmigui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 *
 * @author ronan
 */
public class List_render extends DefaultListCellRenderer
{
    
       @Override
       public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        
        setText(value.toString());

        if (index % 2 == 0)
        {
            setBackground(new Color(237,241,242));
        }
        else 
        {
            setBackground(Color.WHITE);
        }
        return this;
        
        
    }
    
}
