package Client;

import Core.MyDatagramSocket;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This example illustrates a process which sends then receives using a datagram
 * socket.
 */
public class Client {
    // An application which sends then receives a message using
    // connectionless datagram socket.

    public static void main(String[] args) {
        try {
            ArrayList<String> responses = new ArrayList<String>();
            // Scanner for interaction with the user
            Scanner input = new Scanner(System.in);
            
            // Network code set up - establish address information: 
            // IP and port of recipient, plus our own port for listening on)
            
            InetAddress receiverHost = InetAddress.getByName("localhost");
            int receiverPort = Integer.parseInt("10313");
            int myPort = Integer.parseInt("1211");
            
            // Set up variable to store data being transmitted
            String message = "from sender";

            // Set up socket - create the connection point we will listen on and send data through
            // Note: This socket is connected to OUR port, the port *this* program will listen on
            //       The Datagram Packet will be addressed with the port of the OTHER program
            MyDatagramSocket mySocket = new MyDatagramSocket(myPort);

            // Loop STARTS here
            // While we have not received the agreed-upon termination code
            while(!message.equals("-1"))
            {
                System.out.print("Enter message:> ");
                message = input.nextLine();
                
                // send message to the receiver (located at port receiverPort on receiverHost computer)
                mySocket.sendMessage(receiverHost, receiverPort, message);
                
                // Check if message we just sent was a termination code
                // If not a termination, try to receive a response
                if(!message.equals("-1"))
                {
                    // First wait to receive a datagram from the socket
                    DatagramPacket datagram = mySocket.receiveMessage();
                    // Display the received message to the user
                    message = new String(datagram.getData());
                    System.out.println("From sender:> " + message);
                    responses.add(message);
                }
            }
            // Termination has been requested - close the socket.
            mySocket.close();
            
            System.out.println(responses);
            System.out.println("Connection terminated");
        } // end try
        catch (Exception ex) {
            ex.printStackTrace();
        } //end catch
    } //end main
} //end class
