package Server;

import Core.MyDatagramSocket;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

/**
 * This example illustrates a process which sends then receives using a datagram
 * socket.
 */
public class Server {
    // An application which sends then receives a message using
    // connectionless datagram socket.

    public static void main(String[] args) {
        try {
            // Scanner for interaction with the user
            Scanner input = new Scanner(System.in);
            
            // Network code set up - establish address information (IP and port of recipient, plus our own port for listening on)
            InetAddress receiverHost = InetAddress.getByName("localhost");
            int receiverPort = Integer.parseInt("0");
            int myPort = Integer.parseInt("10313");
            
            // Set up variable to store data being transmitted
            String message = "from receiver";

            // Set up socket - create the connection point we will listen on and send data through
            // Note: This socket is connected to OUR port, the port *this* program will listen on
            //       The Datagram Packet will be addressed with the port of the OTHER program
            MyDatagramSocket mySocket = new MyDatagramSocket(myPort);
            
            System.out.println("Now waiting for first message");
            // LOOP BEGINS
            // While we have not received the agreed-upon termination code
            while (true) 
            {
                // First wait to receive a datagram from the socket
                DatagramPacket datagram = mySocket.receiveMessage();
                receiverHost = datagram.getAddress();
                receiverPort = datagram.getPort();
                
                message = new String (datagram.getData());
                // Display the received message to the user
                System.out.println("From sender:> " + message);
                // Check if message we just received was a termination code
                // If not a termination, take in response and send back
                System.out.println(message.substring(0,6));
                if(message.startsWith("echo "))
                {
                    message = message.substring(5, message.length());
                    mySocket.sendMessage(receiverHost, receiverPort, message);
                }
                else if(message.trim().equals("random"))
                {
                    Random rn = new Random();
                    message = "" + rn.nextInt(1000) + 1; 
                    mySocket.sendMessage(receiverHost, receiverPort, message);
                }
                else if(message.trim().equals("daytime"))
                {
                    Calendar c1 = Calendar.getInstance();
                    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    message = date.format(c1.getTime());
                    mySocket.sendMessage(receiverHost, receiverPort, message);
                }
                else if (!message.trim().equals("-1"))
                {
                    
                    mySocket.sendMessage(receiverHost, receiverPort, "-----INVALID COMMAND ----");
                }
                                
            }
            
            // Termination has been requested - close the socket.
            
          
        } // end try
        catch (Exception ex) {
            ex.printStackTrace();
        } //end catch
    } //end main
} //end class
