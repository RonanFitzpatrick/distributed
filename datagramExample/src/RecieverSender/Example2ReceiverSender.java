package RecieverSender;
import Core.MyDatagramSocket;
import java.net.*;

/**
* This example illustrates a process which sends then receives
* using a datagram socket.
*/
public class Example2ReceiverSender
{
	// An application which sends then receives a message using
	// connectionless datagram socket.

	public static void main(String[] args)
	{
		try
		{
			InetAddress receiverHost = InetAddress.getByName("localhost");
			int receiverPort = Integer.parseInt("1000");
			int myPort = Integer.parseInt("1234");
			String message = "from receiver";

			// instantiates a datagram socket for both sending and receiving data
			MyDatagramSocket mySocket = new MyDatagramSocket(myPort);
			
			// First wait to receive a datagram from the socket
			System.out.println(mySocket.receiveMessage());
			
			// Now send a message to the other process.
			mySocket.sendMessage(receiverHost, receiverPort, message);
			// Message has been sent - close the socket.
			mySocket.close();
		} // end try
		catch (Exception ex)
		{
			ex.printStackTrace();
		} //end catch
	} //end main
} //end class
