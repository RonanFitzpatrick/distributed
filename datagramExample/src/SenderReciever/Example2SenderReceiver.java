package SenderReciever;
import Core.MyDatagramSocket;
import java.net.*;

/**
 * This example illustrates a process which sends then receives
 * using a datagram socket.
 */
public class Example2SenderReceiver
{
	// An application which sends then receives a message using
	// connectionless datagram socket.

	public static void main(String[] args)
	{
		try
		{
			InetAddress receiverHost = InetAddress.getByName("localhost");
			int receiverPort = Integer.parseInt("1234");
			int myPort = Integer.parseInt("1000");
			String message = "from sender";

			// instantiates a datagram socket for both sending and receiving data
			MyDatagramSocket mySocket = new MyDatagramSocket(myPort);
			
			// send message to the receiver (located at port receiverPort on receiverHost computer)
			mySocket.sendMessage(receiverHost, receiverPort, message);
			
			// now wait to receive a datagram from the socket & display it to the user
			System.out.println(mySocket.receiveMessage());
			
			// Message has been receiver - close the socket.
			mySocket.close();
		} // end try
		catch (Exception ex)
		{
			ex.printStackTrace();
		} //end catch
	} //end main
} //end class
