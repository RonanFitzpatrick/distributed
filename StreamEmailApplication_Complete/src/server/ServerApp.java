package server;

import core.Email;
import core.EmailUtility;
import core.MySocket;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class ServerApp
{
    private EmailStore theEmailStore;

    public static void main(String[] args)
    {
        ServerApp serverApp = new ServerApp();
        serverApp.startApp();
    }

    public void startApp()
    {
        //used to store emails
        this.theEmailStore = new EmailStore();

        try
        {
            System.out.println("---- SERVER ----");
            // Create listening socket to accept connections through
            ServerSocket serverSocket = new ServerSocket(EmailUtility.SERVER_PORT);
            System.out.println("Waiting...");
            while(true)
            {
                // Accept next client
                MySocket client = new MySocket(serverSocket.accept());

                // Interact with client
                executeSession(client);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void executeSession(MySocket client)
    {
        // Do code for specific client's session
        System.out.println("New client on " + client.getRemoteAddress());

        String message = "";
        // While client is still interacting
        while(!message.equals(EmailUtility.EXIT))
        {
            // Receive command
            message = client.receiveMessage();
            String response = executeCommand(message);
            client.sendMessage(response);
        }
    }

    private String executeCommand(String msg)
    {
        String[] msgArray = msg.split(EmailUtility.COMMAND_BREAKING_CHAR);
        String response = "";
        if(msgArray[0].equals(EmailUtility.SEND_MAIL))
        {
            //Parse and store email
            Email e = EmailUtility.parseEmail(msgArray[1]);
            if(e != null)
            {
                theEmailStore.add(e);
                response = EmailUtility.SUCCESSFUL_SEND;
            }
            else
            {
                response = EmailUtility.FAILED_SEND;
            }
        }
        else if(msgArray[0].equals(EmailUtility.VIEW_MAIL) || msgArray[0].equals(EmailUtility.VIEW_SENT))
        {
            ArrayList<Email> mails = null;
            if(msgArray[0].equals(EmailUtility.VIEW_MAIL))
            {
                // Search for and send back received email
                mails = theEmailStore.getReceivedEmails(msgArray[1]);
            }
            else
            {
                // Search for and send back sent email
                mails = theEmailStore.getSentEmails(msgArray[1]);
            }
            
            if(mails.isEmpty())
            {
                response = EmailUtility.NO_MAIL;
            }
            else
            {
                // Format message to send back to client
                StringBuffer sb = new StringBuffer();
                sb.append(mails.get(0).getTransmisionFormat());
                for(int i = 1; i < mails.size(); i++)
                {
                    sb.append(EmailUtility.EMAIL_SEPARATOR_CHAR);
                    sb.append(mails.get(i).getTransmisionFormat());
                }
                response = sb.toString();
            }
        }
        else if(msgArray[0].equals(EmailUtility.EXIT))
        {
            response = EmailUtility.EXIT_ACCEPTED;
        }

        //to do...
        return response;
    }
}
