package server;

import core.Email;
import java.util.ArrayList;
import java.util.Objects;

public class EmailStore
{
    private ArrayList<Email> emailList;

    public EmailStore()
    {
        emailList = new ArrayList<Email>();
    }

    public void add(Email email)
    {
        emailList.add(email);
    }

    public void clear()
    {
        emailList.clear();
    }

    public void size()
    {
        emailList.size();
    }

    public String toString()
    {
        return "EmailStore [emailList=" + emailList + "]";
    }
    
    public ArrayList<Email> getSentEmails(String sender)
    {
        ArrayList<Email> mails = new ArrayList();
        for(Email e : emailList)
        {
            if(e.getFrom().equalsIgnoreCase(sender))
            {
                mails.add(e);
            }
        }
        return mails;
    }

    public ArrayList<Email> getReceivedEmails(String recipient)
    {
        ArrayList<Email> mails = new ArrayList();
        for(Email e : emailList)
        {
            if(e.getTo().equalsIgnoreCase(recipient))
            {
                mails.add(e);
            }
        }
        return mails;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.emailList);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final EmailStore other = (EmailStore) obj;
        if (!Objects.equals(this.emailList, other.emailList))
        {
            return false;
        }
        return true;
    }
}
