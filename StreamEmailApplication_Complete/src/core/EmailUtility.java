package core;

import java.util.ArrayList;

/**
 *
 * @author Michelle
 */
public class EmailUtility 
{
    public final static int SERVER_PORT = 5000;
    
    public final static String SEND_MAIL = "send";
    public final static String SUCCESSFUL_SEND = "SUCCESSFUL";
    public final static String FAILED_SEND = "FAILED";
    
    public final static String VIEW_MAIL = "getMail";
    public final static String NO_MAIL = "NONE";
    
    public final static String VIEW_SENT = "getSent";
    
    public final static String EXIT = "exit";
    public final static String EXIT_ACCEPTED = "goodbye";
    
    public final static String COMMAND_BREAKING_CHAR = "%%%";
    public final static String EMAIL_COMPONENT_BREAKING_CHAR = "¬¬";
    public final static String EMAIL_SEPARATOR_CHAR = "~~";
    
    public static ArrayList<Email> parseEmails(String [] emails)
    {
        ArrayList<Email> mailList = new ArrayList();
        for(String email : emails)
        {
            Email mail = parseEmail(email);
            mailList.add(mail);
        }
        
        return mailList;
    }
    
    public static Email parseEmail(String email)
    {
        String [] components = email.split(EMAIL_COMPONENT_BREAKING_CHAR);
        // Structure: sender¬¬recipient¬¬subject¬¬body
        if(components.length == 4)
        {
            // Make an email from the components of the String
            Email e = new Email(components[0], components[1], components[2], components[3]);
            return e;
        }
        return null;
    }
}
